import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tripegate/models/flight.dart';
import 'package:tripegate/models/flight_location.dart';
import 'package:tripegate/pages/tripegate_form_page.dart';
import 'package:tripegate/provider/flight_provider.dart';

void main() {
  runApp(
    MultiProvider(
      providers: [
        ChangeNotifierProvider<FlightProvider>(
          create: (context) => FlightProvider(
              1,
              Flight(
                "",
                "",
              ),
              [FlightLocation(1, "", "", "")]),
        ),
      ],
      child: const TripeGateApp(),
    ),
  );
}

class TripeGateApp extends StatelessWidget {
  const TripeGateApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'TripeGate',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const TripeGateFromPage(),
    );
  }
}
