import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tripegate/models/flight.dart';
import 'package:tripegate/models/flight_location.dart';
import 'package:tripegate/provider/flight_provider.dart';

class TripeGateFromPage extends StatefulWidget {
  const TripeGateFromPage({Key? key}) : super(key: key);

  @override
  State<TripeGateFromPage> createState() => _TripeGateFromPageState();
}

class _TripeGateFromPageState extends State<TripeGateFromPage> {
  GlobalKey<FormState>? _formKey;

  @override
  void initState() {
    super.initState();
    _formKey = GlobalKey<FormState>();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        automaticallyImplyLeading: false,
        title: RichText(
          text: const TextSpan(
            children: [
              TextSpan(
                text: 'Flight',
                style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.normal,
                    color: Colors.white70),
              ),
              TextSpan(
                text: 'Search',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 18,
                ),
              ),
            ],
          ),
        ),
        leading: const Icon(
          Icons.arrow_back_outlined,
          color: Colors.black,
        ),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Consumer<FlightProvider>(
            builder: (_, mProvider, __) {
              return Form(
                key: _formKey,
                child: Column(
                  children: [
                    ListView.builder(
                      physics: const NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      itemCount: mProvider.getFlightLocations()!.length,
                      itemBuilder: (context, index) {
                        return _locationItemListWidget(
                            index: index,
                            provider: mProvider,
                            flightLocation:
                                mProvider.getFlightLocations()![index]);
                      },
                    ),
                    Row(
                      children: [
                        Expanded(
                          child: TextFormField(
                            keyboardType: TextInputType.number,
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'Please number of traveller';
                              }
                              return null;
                            },
                            decoration: const InputDecoration(
                              hintText: "TRAVELLERS",
                              hintStyle: TextStyle(color: Colors.grey),
                            ),
                            onChanged: (val) {
                              //_onFlightUpdate(val: val, fieldName: "traveller");
                            },
                          ),
                        ),
                        const SizedBox(
                          width: 16,
                        ),
                        Expanded(
                          child: TextFormField(
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'Please flight class';
                              }
                              return null;
                            },
                            decoration: const InputDecoration(
                              hintText: "Class",
                              hintStyle: TextStyle(color: Colors.grey),
                            ),
                            onChanged: (val) {
                              //_onFlightUpdate(val: val, fieldName: "class");
                            },
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 32,
                    ),
                    ElevatedButton(
                      onPressed: () {
                        if (_formKey!.currentState!.validate()) {
                          Flight flight = mProvider.getFlight();
                          print(flight.flightClass);
                          //TODO
                          // In _flight object you will get all the given input value
                          // Just pass to your viewmodel/netowrk via anything
                        }
                      },
                      child: Text('Search'.toUpperCase()),
                      style: ElevatedButton.styleFrom(
                        shape: const CircleBorder(),
                        padding: const EdgeInsets.all(24),
                      ),
                    )
                  ],
                ),
              );
            },
          ),
        ),
      ),
    );
  }

  _locationItemListWidget(
      {int? index, FlightProvider? provider, FlightLocation? flightLocation}) {
    return Column(
      children: [
        TextFormField(
          validator: (value) {
            if (value == null || value.isEmpty) {
              return 'Please enter from city name';
            }
            return null;
          },
          decoration: const InputDecoration(
            hintText: "From",
            hintStyle: TextStyle(color: Colors.grey),
          ),
          onChanged: (val) {
            _onLocationUpdate(
                index: index,
                val: val,
                fieldName: "fromCity",
                provider: provider);
          },
        ),
        TextFormField(
          validator: (value) {
            if (value == null || value.isEmpty) {
              return 'Please enter To city name';
            }
            return null;
          },
          decoration: const InputDecoration(
            hintText: "To",
            hintStyle: TextStyle(color: Colors.grey),
          ),
          onChanged: (val) {
            _onLocationUpdate(
                index: index,
                val: val,
                fieldName: "toCity",
                provider: provider);
          },
        ),
        TextFormField(
          validator: (value) {
            if (value == null || value.isEmpty) {
              return 'Please departure date';
            }
            return null;
          },
          decoration: const InputDecoration(
            hintText: "Departure",
            hintStyle: TextStyle(color: Colors.grey),
          ),
          onChanged: (val) {
            _onLocationUpdate(
                index: index,
                val: val,
                fieldName: "departureDate",
                provider: provider);
          },
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Visibility(
              visible: provider!.getFlightLocations()!.length - 1 == index
                  ? true
                  : false,
              child: InkWell(
                onTap: () {
                  setState(() {
                    _onAdd(index: index!, provider: provider);
                  });
                },
                child: const Icon(
                  Icons.add_circle,
                  size: 28,
                  color: Colors.green,
                ),
              ),
            ),
            Visibility(
              visible: provider.getFlightLocations()!.length > 1 ? true : false,
              child: InkWell(
                child: const Icon(
                  Icons.remove_circle,
                  size: 28,
                  color: Colors.red,
                ),
                onTap: () {
                  _onRemove(index: index, provider: provider);
                },
              ),
            ),
          ],
        ),
      ],
    );
  }

  _onLocationUpdate(
      {int? index, String? val, String? fieldName, FlightProvider? provider}) {
    provider!.setFlightLocationValue(index, val, fieldName);
  }

  _onAdd({int? index, FlightProvider? provider}) {
    provider!.increaseFormCount(1);
    provider.setDefaultFlightLocationValue();
  }

  _onRemove({int? index, FlightProvider? provider}) {
    provider!.removeFlightLocation(index!);
  }
}
