import 'package:tripegate/models/flight_location.dart';

class Flight {
  String? _travellerNumber;
  String? flightClass;
  List<FlightLocation>? _flightLocations;

  Flight(this._travellerNumber, this.flightClass);

  String? getTravellerNumber() {
    return _travellerNumber;
  }

  void setTravellerNumber(String value) {
    _travellerNumber = value;
  }

  String? getFlightClass() {
    return flightClass;
  }

  void setFlightClass(String value) {
    flightClass = value;
  }

  List<FlightLocation>? getFlightLocations() {
    return _flightLocations;
  }

  void setFlightLocations(List<FlightLocation> list) {
    _flightLocations = list;
  }
}
