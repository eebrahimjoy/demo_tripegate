class FlightLocation {
  int? _id;
  String? _fromCity;
  String? _toCity;
  String? _departureDate;

  FlightLocation(this._id, this._fromCity, this._toCity, this._departureDate);

  String? getDepartureDate() {
    return _departureDate;
  }

  void setDepartureDate(String value) {
    _departureDate = value;
  }

  String? getToCity() {
    return _toCity;
  }

  void setToCity(String value) {
    _toCity = value;
  }

  String? getFromCity() {
    return _fromCity;
  }

  void setFromCity(String value) {
    _fromCity = value;
  }

  int? getId() {
    return _id;
  }

  void setId(int value) {
    _id = value;
  }



}
