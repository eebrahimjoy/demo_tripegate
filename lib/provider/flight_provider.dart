import 'package:flutter/cupertino.dart';
import 'package:tripegate/models/flight.dart';
import 'package:tripegate/models/flight_location.dart';

class FlightProvider with ChangeNotifier {
  int? _formCount;
  Flight? _flight;
  List<FlightLocation>? _flightLocations;

  FlightProvider(this._formCount, this._flight, this._flightLocations);

  void increaseFormCount(int? value) {
    _formCount = (_formCount! + value!);
    notifyListeners();
  }

  int? getFormCount() {
    return _formCount;
  }

  List<FlightLocation>? getFlightLocations() {
    return _flightLocations;
  }

  void addFlightLocations(List<FlightLocation> list) {
    _flightLocations = list;
    notifyListeners();
  }

  void setFlightLocationValue(int? index, String? val, String? fieldName) {
    if (fieldName == "fromCity") {
      _flightLocations![index!].setFromCity(val!);
    } else if (fieldName == "toCity") {
      _flightLocations![index!].setToCity(val!);
    } else if (fieldName == "departureDate") {
      _flightLocations![index!].setDepartureDate(val!);
    }
    _flight!.setFlightLocations(_flightLocations!);

    notifyListeners();
  }

  void setDefaultFlightLocationValue() {
    _flightLocations!.add(FlightLocation(getFormCount(), "", "", ""));
    notifyListeners();
  }

  void removeFlightLocation(int index) {
    _flightLocations!.removeAt(index);
    notifyListeners();
  }

  Flight getFlight() {
    return _flight!;
  }
}
